use crate::configuration::ConfigurationServer;
use crate::errors::PeauError;
use crate::tls;
use actix_http::Request;
use actix_service::IntoServiceFactory;
use actix_service::ServiceFactory;
use actix_web::dev::AppConfig;
use actix_web::dev::MessageBody;
use actix_web::dev::Service;
use actix_web::error::Error;
use actix_web::web::HttpResponse as Response;
use actix_web::HttpServer;
use std::fmt::Debug;

/// Bing a web server providing a configuration an initialized web server object
///
/// # Arguments
/// * `conf` - The server configuration
/// * `server` - The initialized web server
///
/// # Return value
/// The binded webserver
pub fn bind_web_server<F, I, S, B>(
    conf: &ConfigurationServer,
    server: HttpServer<F, I, S, B>,
) -> Result<HttpServer<F, I, S, B>, PeauError>
where
    F: Fn() -> I + Send + Clone + 'static,
    I: IntoServiceFactory<S>,
    S: ServiceFactory<Config = AppConfig, Request = Request>,
    S::Error: Into<Error> + 'static,
    S::InitError: Debug,
    S::Response: Into<Response<B>> + 'static,
    <S::Service as Service>::Future: 'static,
    B: MessageBody + 'static,
{
    let mut server = server
        .workers(conf.worker_threads as usize)
        .max_connections(conf.max_connections as usize)
        .client_timeout(conf.request_timeout as u64)
        .shutdown_timeout(conf.shutdown_timeout as u64);
    let binding = format!("{}:{}", conf.host, conf.port);
    if conf.tls.is_some() {
        server = server.bind_rustls(binding, tls::tls_config(&conf)?)?;
    } else {
        server = server.bind(binding)?;
    }
    Ok(server)
}
