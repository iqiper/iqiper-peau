use crate::configuration::ConfigurationServerDistant;
use crate::errors::PeauError;
use std::fs;
use std::time::Duration;

/// Init a new reqwest client
///
/// # Arguments
/// * `config` - The distant server configuration
///
/// # Return value
/// The reqwest client
pub fn init_reqwest_client(
    config: &ConfigurationServerDistant,
) -> Result<reqwest::Client, PeauError> {
    let ca;
    let identity;
    let mut client_build = reqwest::ClientBuilder::new();
    if let Some(timeout) = config.request_timeout {
        client_build = client_build.timeout(Duration::from_millis(timeout));
    }
    if let Some(tls) = &config.tls {
        let ca_string = fs::read_to_string(tls.ca_certificate.as_str())
            .map_err(|e| PeauError::from_io(e, tls.ca_certificate.as_str()))?;
        ca = reqwest::Certificate::from_pem(ca_string.as_bytes())?;
        if let Some(mtls) = &tls.mtls {
            let crt = fs::read_to_string(mtls.certificate.as_str())
                .map_err(|e| PeauError::from_io(e, mtls.certificate.as_str()))?;
            let key = fs::read_to_string(mtls.key.as_str())
                .map_err(|e| PeauError::from_io(e, mtls.key.as_str()))?;
            identity =
                reqwest::Identity::from_pem(format!("{}\n{}\n{}", key, crt, ca_string).as_bytes())?;
            client_build = client_build.identity(identity).add_root_certificate(ca);
            Ok(client_build.build()?)
        } else {
            client_build = client_build.add_root_certificate(ca);
            Ok(client_build.build()?)
        }
    } else {
        Ok(client_build.build()?)
    }
}

/// Init a new reqwest client (blocking)
///
/// # Arguments
/// * `config` - The distant server configuration
///
/// # Return value
/// The reqwest client (blocking)
pub fn init_reqwest_client_blocking(
    config: &ConfigurationServerDistant,
) -> Result<reqwest::blocking::Client, PeauError> {
    let ca;
    let identity;
    let mut client_build = reqwest::blocking::ClientBuilder::new();
    if let Some(timeout) = config.request_timeout {
        client_build = client_build.timeout(Duration::from_millis(timeout));
    }
    if let Some(tls) = &config.tls {
        let ca_string = fs::read_to_string(tls.ca_certificate.as_str())
            .map_err(|e| PeauError::from_io(e, tls.ca_certificate.as_str()))?;
        ca = reqwest::Certificate::from_pem(ca_string.as_bytes())?;
        if let Some(mtls) = &tls.mtls {
            let crt = fs::read_to_string(mtls.certificate.as_str())
                .map_err(|e| PeauError::from_io(e, mtls.certificate.as_str()))?;
            let key = fs::read_to_string(mtls.key.as_str())
                .map_err(|e| PeauError::from_io(e, mtls.key.as_str()))?;
            identity =
                reqwest::Identity::from_pem(format!("{}\n{}\n{}", key, crt, ca_string).as_bytes())?;
            client_build = client_build.identity(identity).add_root_certificate(ca);
            Ok(client_build.build()?)
        } else {
            client_build = client_build.add_root_certificate(ca);
            Ok(client_build.build()?)
        }
    } else {
        Ok(client_build.build()?)
    }
}
