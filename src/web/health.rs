use actix_web::http::StatusCode;
use actix_web::{web, HttpResponse};
use once_cell::sync::Lazy;
use std::sync::RwLock;

static READY_SEMAPHORE: Lazy<RwLock<bool>> = Lazy::new(|| RwLock::new(false));

static ALIVE_SEMAPHORE: Lazy<RwLock<bool>> = Lazy::new(|| RwLock::new(false));

enum CheckType {
    Ready,
    Alive,
}

/// Check if the web server is ready/alive
///
/// # Arguments
/// * `t` - The type of check beeing executed (ready or alive)
///
/// # Return value
/// An HTTP response
/// 200 On ready/alive
/// 503 On not ready/not alive
fn check(t: CheckType) -> HttpResponse {
    match t {
        CheckType::Alive => match ALIVE_SEMAPHORE.read() {
            Ok(v) => match *v {
                true => HttpResponse::build(StatusCode::OK).finish(),
                false => HttpResponse::build(StatusCode::SERVICE_UNAVAILABLE).finish(),
            },
            Err(x) => panic!("The alive semaphore is corrupted {}", x),
        },
        CheckType::Ready => match READY_SEMAPHORE.read() {
            Ok(v) => match *v {
                true => HttpResponse::build(StatusCode::OK).finish(),
                false => HttpResponse::build(StatusCode::SERVICE_UNAVAILABLE).finish(),
            },
            Err(x) => panic!("The alive semaphore is corrupted {}", x),
        },
    }
}

/// Change the readyness status
pub fn set_ready() {
    *READY_SEMAPHORE
        .write()
        .expect("The ready semaphore is corrupted") = true;
}

/// Change the readyness status
pub fn set_alive(v: bool) {
    *ALIVE_SEMAPHORE
        .write()
        .expect("The ready semaphore is corrupted") = v;
}

/// Display 200 when the server is ready on `/-/ready`
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn ready(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/-/ready")
            .route(web::get().to(|| check(CheckType::Ready)))
            .route(web::head().to(|| check(CheckType::Ready))),
    );
}

/// Display 200 when the server is liveness on `/-/alive`
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn alive(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/-/alive")
            .route(web::get().to(|| check(CheckType::Alive)))
            .route(web::head().to(|| check(CheckType::Alive))),
    );
}
