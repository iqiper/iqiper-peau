use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConfigurationServer {
    pub host: String,
    pub port: u32,
    pub advertise_scheme: String,
    pub advertise_uri: String,
    pub worker_threads: u32,
    pub max_connections: u32,
    pub request_timeout: u32,
    pub shutdown_timeout: u32,
    pub tls: Option<ConfigurationCerts>,
    pub ca_certificate: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConfigurationServerDistant {
    pub scheme: String,
    pub host: String,
    pub port: u32,
    pub request_timeout: Option<u64>,
    pub tls: Option<ConfigurationTLS>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConfigurationCerts {
    pub key: String,
    pub certificate: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConfigurationTLS {
    pub ca_certificate: String,
    pub mtls: Option<ConfigurationCerts>,
}
