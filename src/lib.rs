//! `iqiper-peau` is a boilerplate lib used to quickly setup other services.
//!
//! ## Web server
//!
//! A web server can be easily deployed with possible tls configured. The configuration
//! is part of this project.
//! The server binded by this lib could also expose some health endpoins to check if
//! the server is ready/alive.

pub mod configuration;
pub mod errors;
pub mod tls;
pub mod web;

pub extern crate actix_http;
pub extern crate actix_rt;
pub extern crate actix_web;
pub extern crate reqwest;
pub extern crate rustls;
pub extern crate actix_web_prom;
pub extern crate prometheus;

pub use web::web as server;
