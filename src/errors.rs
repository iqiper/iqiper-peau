use std::{fmt, io};

/// Wrap all kind of error that could be encoutered by the service.
#[derive(Debug)]
pub enum PeauError {
    /// Wrap an IO error with the name of the file or output linked to the error.
    IO(String, io::Error),
    /// Wrap rustls errors
    TLSError(rustls::TLSError),
    /// Wrap a request error
    ReqwestError(reqwest::Error),
}

impl fmt::Display for PeauError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            PeauError::IO(filename, io_err) => write!(f, "{}: {}", filename, io_err),
            PeauError::TLSError(err) => write!(f, "{}", err),
            PeauError::ReqwestError(err) => write!(f, "{}", err),
        }
    }
}

impl std::error::Error for PeauError {}

impl PeauError {
    /// Generate from IO
    ///
    /// # Arguments
    /// * `err` - The Serde Json error to wrap.
    /// * `filename` - the name of the file which caused the error.
    ///
    /// # Return Value
    /// The Error wrap.
    pub fn from_io(err: io::Error, filename: &str) -> Self {
        PeauError::IO(filename.to_owned(), err)
    }
}

impl From<rustls::TLSError> for PeauError {
    fn from(err: rustls::TLSError) -> Self {
        PeauError::TLSError(err)
    }
}

impl From<std::io::Error> for PeauError {
    fn from(err: std::io::Error) -> Self {
        PeauError::IO("[none]".to_string(), err)
    }
}

impl From<reqwest::Error> for PeauError {
    fn from(err: reqwest::Error) -> Self {
        PeauError::ReqwestError(err)
    }
}
