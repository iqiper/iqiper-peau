use crate::configuration::ConfigurationServer;
use crate::errors::PeauError;
use std::fs::File;
use std::io::BufReader;

/// Create a rustls configuration from a `peau` server configuration
///
/// # Arguments
/// * `config` - The server configuration
///
/// # Return value
/// The rustls server config
pub fn tls_config(settings: &ConfigurationServer) -> Result<rustls::ServerConfig, PeauError> {
    let mut store = rustls::RootCertStore::empty();
    if let Some(ca_certificate) = settings.ca_certificate.as_ref() {
        let file =
            File::open(ca_certificate).map_err(|err| PeauError::from_io(err, &ca_certificate))?;
        let mut buf = BufReader::new(file);
        store
            .add_pem_file(&mut buf)
            .expect(&format!("invalid pem file: {}", ca_certificate));
    }
    let cert = if settings.ca_certificate.is_some() {
        rustls::AllowAnyAuthenticatedClient::new(store)
    } else {
        rustls::NoClientAuth::new()
    };
    let mut config = rustls::ServerConfig::new(cert);
    if let Some(tls) = settings.tls.as_ref() {
        let cert_file = &mut BufReader::new(
            File::open(&tls.certificate)
                .map_err(|err| PeauError::from_io(err, &tls.certificate))?,
        );
        let cert_chain = rustls::internal::pemfile::certs(cert_file)
            .expect(&format!("invalid pem file: {}", &tls.certificate));
        let key_file = &mut BufReader::new(
            File::open(&tls.key).map_err(|err| PeauError::from_io(err, &tls.key))?,
        );
        let mut keys = rustls::internal::pemfile::rsa_private_keys(key_file)
            .expect(&format!("invalid pem file: {}", &tls.key));
        config.set_single_cert(cert_chain, keys.remove(0))?;
    }
    Ok(config)
}
